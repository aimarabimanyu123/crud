<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required|integer',
            'bio' => 'required'
        ], 
        [
            'nama.required' => 'Nama tidak boleh kosong',
            'umur.required' => 'Umur tidak boleh kosong',
            'umur.integer' => 'Umur haruslah angka',
            'bio.required' => 'Bio tidak boleh kosong'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'], 
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $casts = DB::table('cast')->get();

        return view('casts.index', compact('casts'));
    }

    public function show($id)
    {
        $casts = DB::table('cast')->where('id', $id)->first();
        
        return view('casts.show', compact('casts'));
    }

    public function edit($id)
    {
        $casts = DB::table('cast')->where('id', $id)->first();
        
        return view('casts.edit', compact('casts'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast,nama|max:255',
            'umur' => 'required|integer',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong',
            'umur.required' => 'Umur tidak boleh kosong',
            'umur.integer' => 'Umur haruslah angka',
            'bio.required' => 'Bio tidak boleh kosong'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'], 
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        
        return redirect('/cast');
    }
}
