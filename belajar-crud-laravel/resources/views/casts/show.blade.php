@extends('layouts.master')

@section('title')
    Detail Cast
@endsection

@section('content')
    <h2>{{$casts->nama}}</h2>
    <h4>{{$casts->umur}}</h4>
    <p>{{$casts->bio}}</p>
@endsection